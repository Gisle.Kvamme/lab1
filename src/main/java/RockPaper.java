import java.util.HashMap;
import java.util.Random;
import java.util.Scanner;


public class RockPaper {
    public static void main(String[] args) {

        //main game reference hashmap
        HashMap<String, String> combos = new HashMap<String, String>();
        combos.put("rock", "paper");
        combos.put("paper", "scissors");
        combos.put("scissors", "rock");

        //tracking variables for print during gameplay
        int roundCounter = 0;
        int humanScore = 0;
        int computerScore = 0;
        String humanChoice = null;
        boolean game = true;

        //scanner initalizing to read user input
        Scanner scan = new Scanner(System.in);

    

        //MAIN GAME LOOP
        while (game) {
            roundCounter ++;

            //user choice with simple input validation
            while (true) {
                System.out.format("Your choice (Rock/Paper/Scissors)?%n");
                humanChoice = scan.next();

                if (!combos.keySet().contains(humanChoice.toLowerCase()))
                    System.out.format("I do not understand %s. Could you try again?%n", humanChoice);
                else
                    break;
            }
            
            
            //getting a random object from a set is annoying, I therefore convert it to an array
            String[] keyArray = (String[]) combos.keySet().toArray(new String[combos.keySet().size()]);
            int randomInt = new Random().nextInt(keyArray.length);
            String computerChoice = keyArray[randomInt];


            //we decide the winner and print he current score
            String choiceStr = String.format("Human chose %s, computer chose %s. ", humanChoice, computerChoice);

            if (humanChoice.equals(computerChoice)) {
                System.out.println(choiceStr + "It's a tie!");
            } else if (humanChoice.equals(combos.get(computerChoice))) {
                System.out.println(choiceStr + "Human wins!");
                humanScore ++;
            } else {
                System.out.println(choiceStr + "Computer wins!");
                computerScore ++;
            }
            System.out.format("Score: human %d, computer %d%n", humanScore, computerScore);


            while (true) {
                System.out.format("Do you wish to continue playing? (y/n)?%n");
                String continueAnswer = scan.next().toLowerCase();
                if (continueAnswer.equals("y")) {
                    break;
                } else if (continueAnswer.equals("n")) {
                    game = false;
                    break;
                } else {
                    System.out.format("I don't understand %s. Try again%n", continueAnswer);
                }
            }

        }
        System.out.println("Bye bye :)");
    }
}
