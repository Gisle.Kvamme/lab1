from random import randint as rint

combos = {"rock" : "paper", "paper" : "scissors", "scissors" : "rock"}  #input gives corresponding losing combination
rcounter, yscore, cscore, selected = 0, 0, 0, False

while True:
    while not selected:
        ychoice = input("Type in 'rock', 'paper' or 'scissors' to choose: ")
        if ychoice not in combos.values():
            print("Incorrect format! Please only use lowercase letters corresponding to 'rock', 'paper', or 'scissors'!")
        else:
            selected = True
            
    print(f"Let's play round {rcounter}")
    cchoice = list(combos.values())[rint(0,2)]
    print(f'I choose {cchoice}!')
    
    if cchoice == combos[ychoice]:
        print("You lose!")
        cscore += 1
    elif ychoice == combos[cchoice]:
        print("You win!")
        yscore += 1
    else:
        print("It's a tie!")
    
    print(f"The score is: YOU {yscore} - ME {cscore}")
    
    
    prompt = input("Type 'q' to quit, otherwise the program will run again: ")
    if prompt == 'q':
        print("Goodbye!")
        break
    
    selected = False
    rcounter += 1
    print()